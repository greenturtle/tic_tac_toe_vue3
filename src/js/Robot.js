class Robot {
    constructor(name, score, opponent) {
        // Properties
        this.name = name
        this.score = score
        this.opponent = opponent
        this.nextMove = 0
        this.moveList = []
        this.validMoveList = [] // Own the indice of "" moves
        this.possibilityList = []
        // Methods
        this.setMoveList = (moveList) => {
            this.moveList = [...moveList] // Dont use just movelist cause its create a new reference to the same data
        }
        this.setValidMoveList = () => {
            this.validMoveList = []
            this.moveList.forEach((move, index) => {
                if(move === ""){
                    this.validMoveList.push(index)
                }
            })
        }
        this.setPossibilityList = (validMove, player) => {
            this.possibilityList = [...this.moveList]
            this.possibilityList[validMove] = player
        }        
        this.setNextMove = () => {
            let win = false
            let counter = false
            this.setValidMoveList()
            if(this.validMoveList.length > 0){
                for(let i = 0; i < this.validMoveList.length; i++){
                    let validMove = this.validMoveList[i]
                    this.setPossibilityList(validMove, this.name)
                    if(this.predictMovement(this.name)){
                        this.nextMove = validMove
                        win = true
                        console.log("WIN")
                        console.log(validMove)
                        this.debug(this.possibilityList)
                        break
                    }
                }
                if(!win) {
                    for(let i = 0; i < this.validMoveList.length; i++){
                        let validMove = this.validMoveList[i]
                        this.setPossibilityList(validMove, this.opponent)
                        if(this.predictMovement(this.opponent)){
                            this.nextMove = validMove
                            counter = true
                            console.log("COUNTER")
                            console.log(validMove)
                            this.debug(this.possibilityList)
                            break
                        }
                    }
                }
                if(!win && !counter){
                    let rand = Number(Math.floor(Math.random() * this.validMoveList.length));
                    this.nextMove = this.validMoveList[rand]  
                }
            }
        }
        this.predictMovement = (playerName) => {
            return (
              this.isEquals( this.possibilityList[0], this.possibilityList[1], this.possibilityList[2], playerName ) || // Start Horizontal
              this.isEquals( this.possibilityList[3], this.possibilityList[4], this.possibilityList[5], playerName ) ||
              this.isEquals( this.possibilityList[6], this.possibilityList[7], this.possibilityList[8], playerName ) || // End Horizontal
              this.isEquals( this.possibilityList[0], this.possibilityList[4], this.possibilityList[8], playerName ) || // Start Diagonal
              this.isEquals( this.possibilityList[2], this.possibilityList[4], this.possibilityList[6], playerName ) || // End Diagonal
              this.isEquals( this.possibilityList[0], this.possibilityList[3], this.possibilityList[6], playerName ) || // Start Vertical
              this.isEquals( this.possibilityList[1], this.possibilityList[4], this.possibilityList[7], playerName ) ||
              this.isEquals( this.possibilityList[2], this.possibilityList[5], this.possibilityList[8], playerName )    // End Vertical
            ) ? true : false
          }
        this.isEquals = (a, b, c, playerName) => {
            const win = (a === b) & (b === c) & (c === playerName) ? true : false
            return win
        }
          this.debug = (moveList) => {
            console.log(`
                Robot = ${this.name}
                MoveLIst
                ${moveList[0]} | ${moveList[1]} | ${moveList[2]}
                ${moveList[3]} | ${moveList[4]} | ${moveList[5]}
                ${moveList[6]} | ${moveList[7]} | ${moveList[8]}
        `);
        }
    }
}


module.exports = Robot

// ANOTATIONS